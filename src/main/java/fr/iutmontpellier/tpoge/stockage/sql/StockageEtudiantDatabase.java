package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Etudiant;
import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StockageEtudiantDatabase implements Stockage<Etudiant> {

    @Override
    public void create(Etudiant element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "INSERT INTO EtudiantsOGE (nom,prenom,idRessourcesFavorite) VALUES (?,?,?)";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setString(1, element.getNom());
            statement.setString(2, element.getPrenom());
            statement.setInt(3, element.getRessourceFavorite().getIdRessource());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Etudiant element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "UPDATE EtudiantsOGE SET nom = ? , prenom = ? , idRessourceFavorite = ? WHERE idEtudiant = ?";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setString(1, element.getNom());
            statement.setString(2, element.getPrenom());
            statement.setInt(3, element.getRessourceFavorite().getIdRessource());
            statement.setInt(4, element.getIdEtudiant());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "DELETE FROM EtudiantsOGE WHERE idEtudiant = ?";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Etudiant getById(int id) {
        Etudiant etudiant = null;
        Ressource ressource = null;
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT idEtudiant, e.nom , prenom ,e.idRessouceFavorite ,r.nom FROM EtudiantsOGE e JOIN RessourcesOGE r ON e.idRessourceFavorite = r.idRessourceFavoritre WHERE idEtudiant = ?" ;
        try (PreparedStatement statement = connection.prepareStatement(requete))
        {
            try (ResultSet result = statement.executeQuery();)
            {
                result.next() ; //On accède à la prochaine ligne
                int idEtudiant = result.getInt( 1);
                String nom = result.getString( 2);
                String prenom = result.getString(3);
                int idRessource = result.getInt( 4);
                String nomRessource = result.getString( 5);


                ressource = new Ressource(nomRessource);
                ressource.setIdRessource(idRessource); ;

                etudiant = new Etudiant(nom,prenom,ressource);
                etudiant.setIdEtudiant(idEtudiant);
            }
        } catch (SQLException e) {
            e.printStackTrace() ;
        }
        return etudiant;
    }

    @Override
    public List<Etudiant> getAll() {
        List<Etudiant> listeEtudiants = new ArrayList<>();
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "SELECT idEtudiant,e.nom , prenom ,e.idRessouceFavorite ,r.nom FROM EtudiantsOGE e JOIN RessourcesOGE r ON e.idRessourceFavorite = r.idRessourceFavoritre";
        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(req);
        )
        {
            while(result.next()) {
                //On accède à la prochaine ligne
                int idEtudiant = result.getInt( 1);
                String nom = result.getString( 2);
                String prenom = result.getString(3);
                int idRessource = result.getInt( 4);
                String nomRessource = result.getString( 5);


                Ressource ressource = new Ressource(nomRessource);
                ressource.setIdRessource(idRessource); ;

                Etudiant etudiant = new Etudiant(nom,prenom,ressource);
                etudiant.setIdEtudiant(idEtudiant);
                listeEtudiants.add(etudiant);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeEtudiants;
    }
}
