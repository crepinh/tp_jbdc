package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockageRessourceDatabase implements Stockage<Ressource> {

@Override
    public void create(Ressource element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "INSERT INTO RessourcesOGE (nom) VALUES (?)";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setString(1, element.getNom());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Ressource element) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "UPDATE RessourcesOGE SET nom = ? WHERE idRessource = ?";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setString(1, element.getNom());
            statement.setInt(2, element.getIdRessource());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "DELETE FROM RessourcesOGE WHERE idRessource = ?";
        try (PreparedStatement statement = connection.prepareStatement(req))
        {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Ressource getById(int id) {
        Ressource ressource = null;
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT idRessource, nom FROM RessourcesOGE WHERE idRessource = ?" ;
        try (PreparedStatement statement = connection.prepareStatement(requete))
        {
            statement.setInt( 1, id);
            try (ResultSet result = statement.executeQuery();)
            {
                result.next() ; //On accède à la prochaine ligne
                int idRessource = result.getInt( 1);
                String nom = result.getString( 2);
                ressource = new Ressource(nom);
                ressource.setIdRessource(idRessource); ;
            }
        } catch (SQLException e) {
            e.printStackTrace() ;
        }
        return ressource;
    }

    @Override
    public List getAll() {
        List<Ressource> listeRessources = new ArrayList<>();
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String req = "SELECT idRessource, nom FROM RessourcesOGE";
        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(req);
        )
        {
            while(result.next()) {
                //On accède à la prochaine ligne
                int id = result.getInt(1);
                String nom = result.getString(2);
                Ressource ressource = new Ressource(nom);
                ressource.setIdRessource(id);
                listeRessources.add(ressource);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeRessources;

    }
}
