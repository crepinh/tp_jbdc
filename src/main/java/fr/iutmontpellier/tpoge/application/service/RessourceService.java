package fr.iutmontpellier.tpoge.application.service;

import com.gasquet.hrepositories.api.EntityRepository;
import com.gasquet.hrepositories.utils.RepositoryManager;
import fr.iutmontpellier.tpoge.metier.entite.Etudiant;
import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;
import fr.iutmontpellier.tpoge.stockage.sql.StockageRessourceDatabase;
import fr.iutmontpellier.tpoge.stockage.stub.StockageRessourceStub;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de service qui permet de gérer différentes ressources (matières)
 * Singleton
 */
public class RessourceService {

    private EntityRepository<Ressource> repository = RepositoryManager.getRepository(Ressource.class);
    //private StockageRessourceDatabase stockageRessource;

    private final static RessourceService INSTANCE = new RessourceService();

    private RessourceService() {
        //stockageRessource = new StockageRessourceDatabase();
    }

    public static RessourceService getInstance() {
        return INSTANCE;
    }

    /**
     * Instancie un objet {@link Ressource} puis le sauvegarde dans la source de données via le repository
     * @param nom : Nom de la {@link Ressource} à créer
     */
    public void createRessource(String nom) {
        Ressource ressource = new Ressource(nom);
        repository.create(ressource);
    }

    /**
     * Récupère une instance de {@link Ressource} depuis la source de données, met à jour son nom puis enregistre la
     * mise à jour de l'entité via le repository
     * @param idRessource : identifiant de la {@link Ressource} à mettre à jour
     * @param nom : nouveau nom pour la {@link Ressource}
     */
    public void updateRessource(int idRessource, String nom) {
        Ressource ressourceAUpdate = repository.findByID(idRessource);
        ressourceAUpdate.setNom(nom);
        repository.update(ressourceAUpdate);
    }

    /**
     * Supprime une {@link Ressource} sur la source de données via le repository
     * @param idRessource : identifiant de la {@link Ressource} à supprimer
     */
    public void deleteRessource(int idRessource) {
//        List<Etudiant> etudiants = EtudiantService.getInstance().getEtudiants();
//        for(Etudiant etudiant : etudiants){
//            if(etudiant.getRessourceFavorite().getIdRessource() == idRessource ){
//                EtudiantService.getInstance().deleteEtudiant(etudiant.getIdEtudiant());
//            }
//        }
        repository.deleteById(idRessource);
    }

    /**
     * Récupère une instance de {@link Ressource} depuis la source de données via le repository
     * @param idRessource : identifiant de la {@link Ressource} à récupérer
     * @return L'instance de {@link Ressource} correspondant à l'identifiant
     */
    public Ressource getRessource(int idRessource) {

        return repository.findByID(idRessource);
    }

    /**
     * Récupère une liste de toutes les {@link Ressource} depuis la source de données via le repository
     * @return La liste de toutes les {@link Ressource}
     */
    public List<Ressource> getRessources() {
        return repository.findAll();
    }
}
